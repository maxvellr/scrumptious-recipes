from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required
# Create your views here.
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipe/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipe/list.html", context)

@login_required
def create_recipe(request):
    if request.method == "POST":
        # we use the for to validate the values
        # and save to the DB
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe=form.save(False)
            recipe.author = request.user
            recipe.save()
            #if all goes well,  we redirect the browser
            #to another page and leave the function
            return redirect("recipe_list")
    else:
        #create instance of Django model class
        form = RecipeForm()
        #give it  context
        context = {
            "form": form
        }
     # render the HTML template with the form
        return render(request, "recipe/create.html", context)

def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm(instance=recipe)

    context = {
        "post_recipe": recipe,
        "post_form": form,
    }
    return render(request, "recipe/edit.html", context)

@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipe/list.html", context)
